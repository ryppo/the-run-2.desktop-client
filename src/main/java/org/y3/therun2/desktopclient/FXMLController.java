package org.y3.therun2.desktopclient;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.springframework.web.client.RestTemplate;
import org.y3.therun2.model.DatabaseContact;

public class FXMLController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
        
        RestTemplate restTemplate = new RestTemplate();
        DatabaseContact[] databaseContacts = restTemplate
                .getForObject("http://therun.tsvliebenburg.de/api.php/therun_contact?transform=1", DatabaseContact[].class);
        for (DatabaseContact databaseContact : databaseContacts) {
            System.out.println("databaseContact: " + databaseContact);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("url: " + url);
        System.out.println("rb: " + rb.getBaseBundleName());
    }    
}
