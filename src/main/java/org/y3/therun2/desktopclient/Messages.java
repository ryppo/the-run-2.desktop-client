package org.y3.therun2.desktopclient;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 *
 * @author christianrybotycky
 */
public class Messages {

    private final String resourceBundleLocation;
    private final ResourceBundle RB;
    
    public Messages(String _resourceBundleLocation) {
        resourceBundleLocation = _resourceBundleLocation;
        RB = ResourceBundle.getBundle(resourceBundleLocation);
    }
    
    public ResourceBundle getBundle() {
        return RB;
    }

    public String getString(String key) {
        try {
            return RB.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
    
    public String THE_RUN_2() {
        return getString("THE_RUN_2");
    }

}
